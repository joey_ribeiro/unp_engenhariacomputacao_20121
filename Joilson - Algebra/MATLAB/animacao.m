function animacao(matriz)

    % Mostrando a matriz
    matriz
    
    % Recebe os indices da matriz (tamanho): 
    % nl = N�mero de Linhas
    % nc = N�mero de Colunas
    [nl,nc] = size(matriz);
    
    for R = 1:nc;
        if R == 1;
           % Adiciona na matrizretorno o primeiro elemento coluna da matriz.
           matrizretorno = [matriz(:, R)];
        else
           % Adiciona na matrizretorno os elementos coluna da matriz, 
           % conforme o valor do for (um a um).
           matrizretorno = [matrizretorno matriz(:, R)];
        end
        if nl == 2;
           plot2d(matrizretorno);
           pause(0.2);
        elseif nl == 3;
           plot3d(matrizretorno);
           pause(0.05);
        end
        
    end
end

