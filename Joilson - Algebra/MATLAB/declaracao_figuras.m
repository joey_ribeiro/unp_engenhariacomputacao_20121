% Quadrado
QUADRADO = [ 0 0; 0 1; 1 1; 1 0; 0 0 ]'

TRIANGULO = [ 0 0; 0 1; 1 1; 0 0]'

% Transformações em 3d
CUBO = [ 0 0 0; 1 0 0; 1 0 1; 0 0 1; 0 0 0; 0 1 0; 0 1 1; 1 1 1; 1 1 0; 0 1 0; 1 1 0; 1 0 0; 1 0 1; 1 1 1; 0 1 1; 0 0 1]'

CASA = [ 1 1 1; 5 1 1; 5 1 4; 3 1 6; 1 1 4; 1 1 1; 1 3 1; 5 3 1; 5 1 1; 5 1 4; 5 3 4; 5 3 1; 5 3 4; 3 3 6; 3 1 6; 3 3 6; 1 3 4; 1 1 4 ; 1 3 4; 1 3 1; 1 1 1; 2 1 1; 2 1 3; 3 1 3; 3 1 1; 2 1 1; 2 0.5 1; 3 0.5 1; 3 1 1; 1 1 1; 1 2 1; 1 2 6; 0 1 6; -1 2 6; -1 2 4; 0 1 4; 1 2 4 ]'