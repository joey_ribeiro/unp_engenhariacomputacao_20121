function demo_transf_linear( matrizTransposta3D )

    % Mostrando a matriz
    matrizTransposta3D

    for R = 0:200
        plot3d(dilatarx3d(dilatary3d(dilatarz3d(matrizTransposta3D, R/200), R/200), R/200));
        pause(0.001);
    end
    
    % PARA X
    % Esticando 
    for R = 100:200
        plot3d(dilatarx3d(matrizTransposta3D, R/100));
        pause(0.001);
    end
    
    % Esticando e contraindo
    for R = 100:200
        plot3d(dilatarx3d(matrizTransposta3D, 200/R));
        pause(0.001);
    end

    % PARA Y
    % Esticando 
    for R = 100:200
        plot3d(dilatary3d(matrizTransposta3D, R/100));
        pause(0.001);
    end
    
    % Esticando e contraindo
    for R = 100:200
        plot3d(dilatary3d(matrizTransposta3D, 200/R));
        pause(0.001);
    end
    
    % PARA Z
    % Esticando 
    for R = 100:200
        plot3d(dilatarz3d(matrizTransposta3D, R/100));
        pause(0.001);
    end
    
    % Esticando e contraindo
    for R = 100:200
        plot3d(dilatarz3d(matrizTransposta3D, 200/R));
        pause(0.001);
    end
    

    % Girando em torno do eixo X
    for anguloEmGraus = 0:360
        plot3d(rotacaox3d(matrizTransposta3D, anguloEmGraus));
        pause(0.001);
    end
    
    % Girando em torno do eixo Y
    for anguloEmGraus = 0: 360
        plot3d(rotacaoy3d(matrizTransposta3D, anguloEmGraus));
        pause(0.001);
    end
    
    % Girando em torno do eixo Z
    for anguloEmGraus = 0: 360
        plot3d(rotacaoz3d(matrizTransposta3D, anguloEmGraus));
        pause(0.001);
    end
    
%    % Combinando dois eixos
%    for anguloEmGraus = 0: 360
%        %Girando em torno do eixo
%        plot3d(rotacaox3d(rotacaoy3d(matrizTransposta3D, anguloEmGraus), anguloEmGraus));
%        pause(0.001);
%
%    end
%
%    % Combinando dois eixos
%    for anguloEmGraus = 0: 360
%        %Girando em torno do eixo
%        plot3d(rotacaoy3d(rotacaoz3d(matrizTransposta3D, anguloEmGraus), anguloEmGraus));
%        pause(0.001);
%
%    end
%
    
    % Juntando todos os tres eixos
    for anguloEmGraus = 0: 360
        plot3d(rotacaox3d(rotacaoy3d(rotacaox3d(matrizTransposta3D, anguloEmGraus), anguloEmGraus), anguloEmGraus));
        pause(0.001);
    end
    
    % Mostrando a imagem dado suas coordenadas transformação do R3 em R2
    plot2d(reflexo3dxy2d(matrizTransposta3D));
    pause(3);
    
    plot2d(reflexo3dyz2d(matrizTransposta3D));
    pause(3);
    
    plot2d(reflexo3dxz2d(matrizTransposta3D));
    pause(3);
    
    % Guardando a matriz 2d para poder trabalhar com ela
    Matriz2D = reflexo3dxz2d(matrizTransposta3D)
    
    % Girando o 2D
    for R = 0:360
        plot2d(rotacao2d(Matriz2D, R));
        pause(0.001);
    end
    
    % Aumentando 3/4 da imagem
    plot2d(dilatar2d(Matriz2D, 0.75));
    pause(3);
    
    plot2d(reflexaox2d(Matriz2D));
    pause(3);
    
    plot2d(reflexaoy2d(Matriz2D));
    pause(3);
    
    plot2d(reflexaoreta2d_YX(Matriz2D));
    pause(3);
    
    % Finalize setando a imagem em 3d da casa
    plot3d(matrizTransposta3D);
    
end

