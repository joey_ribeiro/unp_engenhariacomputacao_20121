function Retorno = dilatar2d(MatrizTransposta, fator);
    Transformacao = fator*[ 1 0 0 ; 0 1 0; 0 0 1]';
    Retorno = Transformacao*MatrizTransposta;
end

