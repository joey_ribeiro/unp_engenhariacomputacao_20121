function Retorno = dilatarx3d(MatrizTransposta, fator);
    Transformacao = [ fator 0 0 ; 0 1 0; 0 0 1];
    Retorno = Transformacao*MatrizTransposta;
end
