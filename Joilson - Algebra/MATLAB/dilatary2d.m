function Retorno = dilatary2d(MatrizTransposta, fator);
    Transformacao = [ 1 0; 0 fator]';
    Retorno = Transformacao*MatrizTransposta;
end
