% Monta uma imagem colorida dado uma matriz Transposta
function plot2dcolorido (MatrizTransposta);
    
    Matriz = MatrizTransposta';
    X = Matriz(:,1);
    Y = Matriz(:,2);
    
    patch(X, Y, 'y');
    xlim([-1 8]); ylim([-1 8]);
end