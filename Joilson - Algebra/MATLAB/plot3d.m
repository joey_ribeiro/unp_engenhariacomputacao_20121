function plot3d (MatrizTransposta)

    Matriz = MatrizTransposta';
    X = Matriz(:,1);
    Y = Matriz(:,2);
    Z = Matriz(:,3);
    
    plot3(X, Y, Z);
    xlim([-8 8]); ylim([-8 8]); zlim([-8 8])
    grid
end