function MatrizRetorno = reflexaox2d( matrizTransposta )

    transformacao = [ 1 0; 0 -1 ]';
    MatrizRetorno = transformacao*matrizTransposta;

end