function Retorno = reflexo3dxy2d(MatrizTransposta);
    Transformacao = [ 1 0 ; 0 1; 0 0 ]';
    
    Retorno = Transformacao*MatrizTransposta;
end
