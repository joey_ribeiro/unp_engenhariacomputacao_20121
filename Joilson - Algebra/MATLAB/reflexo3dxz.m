function Retorno = reflexo3dxz(MatrizTransposta);
    Transformacao = [ 1 0 0; 0 -1 0; 0 0 1 ]';
    
    Retorno = Transformacao*MatrizTransposta;
end
