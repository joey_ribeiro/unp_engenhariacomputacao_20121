function Retorno = reflexo3dyz(MatrizTransposta);
    Transformacao = [ -1 0 0; 0 1 0; 0 0 1 ]';
    
    Retorno = Transformacao*MatrizTransposta;
end
