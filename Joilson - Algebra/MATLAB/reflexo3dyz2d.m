function Retorno = reflexo3dyz2d(MatrizTransposta);
    Transformacao = [ 0 0 ; 1 0; 0 1 ]';
    
    Retorno = Transformacao*MatrizTransposta;
end
