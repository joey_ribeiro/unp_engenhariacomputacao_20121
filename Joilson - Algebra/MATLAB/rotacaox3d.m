% Rotaciona uma matriz em 3d no eixo X dado a matriz transposta e seu �ngulo em Graus
function MatrizRetorno = rotacaox3d( matrizTransposta, anguloGraus )

    % Convertendo o �ngulo em radianos e jogando o valor na vari�vel
    % "anguloRadiano"
    anguloRadiano = (anguloGraus*pi)/180;

    % Prepara a matriz de transforma��o fornecendo o anguloRadiano para
    % dentro das fun��es seno e cosseno
    transformacao = [ 1 0 0; 0 cos(anguloRadiano) -sin(anguloRadiano); 0 sin(anguloRadiano) cos(anguloRadiano) ];

    % A matriz de retorno � o resultado da multiplica��o das matrizes
    % transpostas "transformacao" e "matrizTransposta" que vai resultar na
    % matriz dos vetores coluna rotacionada
    MatrizRetorno = transformacao*matrizTransposta;

end